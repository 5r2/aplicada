* Capacitor
https://en.wikipedia.org/wiki/Equivalent_series_resistance#Capacitors
http://www.capacitorguide.com/parasitic-inductance/
http://referencedesigner.com/books/si/real-capacitor.php
https://www.digikey.com/eewiki/display/Motley/Non-Ideal+Properties+of+Capacitors#Non-IdealPropertiesofCapacitors-DielectricAbsorption
https://www.murata.com/en-us/support/faqs/products/capacitor/mlcc/char/0039
https://forum.digikey.com/t/leakage-of-ceramic-capacitor/2067

* Resistencia
https://www.edn.com/design/components-and-packaging/4423492/Resistors-aren-t-resistors

* Inductor
https://m.eet.com/media/1142818/19256-159688.pdf
http://www.intusoft.com/articles/inductor.pdf

* Cable coaxial
https://catalog.belden.com/techdata/EN/9310_techdata.pdf
http://epuyen.com.ar/sites/default/files/archivos1/hdg-291-_coaxial_rg_58_u_estanado.pdf
https://www.maximintegrated.com/en/app-notes/index.mvp/id/5141
