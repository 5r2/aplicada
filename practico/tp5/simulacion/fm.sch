<Qucs Schematic 0.0.18>
<Properties>
  <View=-20,-20,1030,854,1,0,0>
  <Grid=10,10,1>
  <DataSet=fm.dat>
  <DataDisplay=fm.dpl>
  <OpenDisplay=1>
  <Script=fm.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <_BJT T1 1 660 300 8 -26 0 0 "npn" 1 "1e-16" 1 "1" 1 "1" 0 "0" 0 "0" 0 "0" 1 "0" 0 "0" 0 "1.5" 0 "0" 0 "2" 0 "100" 1 "1" 0 "0" 0 "0" 0 "0" 0 "0" 0 "0" 0 "0" 0 "0.75" 0 "0.33" 0 "0" 0 "0.75" 0 "0.33" 0 "1.0" 0 "0" 0 "0.75" 0 "0" 0 "0.5" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "26.85" 0 "0.0" 0 "1.0" 0 "1.0" 0 "0.0" 0 "1.0" 0 "1.0" 0 "0.0" 0 "0.0" 0 "3.0" 0 "1.11" 0 "26.85" 0 "1.0" 0>
  <GND * 1 880 540 0 0 0 0>
  <GND * 1 400 290 0 0 0 0>
  <Diode D1 0 330 460 15 -26 0 1 "1e-15 A" 1 "1" 1 "10 fF" 1 "0.5" 0 "0.7 V" 0 "0.5" 0 "0.0 fF" 0 "0.0" 0 "2.0" 0 "0.0 Ohm" 0 "0.0 ps" 0 "0" 0 "0.0" 0 "1.0" 0 "1.0" 0 "0" 0 "1 mA" 0 "26.85" 0 "3.0" 0 "1.11" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "26.85" 0 "1.0" 0 "normal" 0>
  <Diode D2 0 330 550 15 -26 1 3 "1e-15 A" 1 "1" 1 "10 fF" 1 "0.5" 0 "0.7 V" 0 "0.5" 0 "0.0 fF" 0 "0.0" 0 "2.0" 0 "0.0 Ohm" 0 "0.0 ps" 0 "0" 0 "0.0" 0 "1.0" 0 "1.0" 0 "0" 0 "1 mA" 0 "26.85" 0 "3.0" 0 "1.11" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "26.85" 0 "1.0" 0 "normal" 0>
  <L L1 1 570 560 10 -26 0 1 "100 nH" 1 "" 0>
  <L L2 1 570 460 10 -26 0 1 "2 uH" 1 "" 0>
  <GND * 1 490 640 0 0 0 0>
  <Vac V3 0 220 540 18 -26 0 1 "1 V" 1 "100 kHz" 1 "0" 0 "0" 0>
  <GND * 0 220 590 0 0 0 0>
  <.TR TR1 1 160 700 0 59 0 0 "lin" 1 "9 us" 1 "10 us" 1 "1001" 1 "Trapezoidal" 0 "2" 0 "1 ns" 0 "1e-16" 0 "150" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "26.85" 0 "1e-3" 0 "1e-6" 0 "1" 0 "CroutLU" 0 "no" 0 "yes" 0 "0" 0>
  <Vdc V2 1 800 140 18 -26 0 1 "12 V" 1>
  <GND * 1 800 190 0 0 0 0>
  <C C2 1 520 350 17 -26 0 1 "10 nF" 1 "" 0 "neutral" 0>
  <R R2 1 520 250 15 -26 0 1 "292 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <R R1 1 660 140 15 -26 0 1 "3200 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <R R3 1 880 500 15 -26 0 1 "50 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <C C1 1 800 410 -26 17 0 0 "28 pF" 1 "" 0 "neutral" 0>
  <C C4 1 400 240 17 -26 0 1 "100 nF" 1 "" 0 "neutral" 0>
  <C C3 1 440 520 17 -26 0 1 "125 pF" 1 "" 0 "neutral" 0>
</Components>
<Wires>
  <660 330 660 410 "" 0 0 0 "">
  <660 410 770 410 "" 0 0 0 "">
  <830 410 880 410 "carga" 890 380 29 "">
  <880 410 880 470 "" 0 0 0 "">
  <520 300 630 300 "" 0 0 0 "">
  <520 300 520 320 "" 0 0 0 "">
  <520 280 520 300 "" 0 0 0 "">
  <660 170 660 200 "" 0 0 0 "">
  <520 200 520 220 "" 0 0 0 "">
  <660 200 660 270 "" 0 0 0 "">
  <520 200 660 200 "" 0 0 0 "">
  <400 200 400 210 "" 0 0 0 "">
  <400 200 520 200 "" 0 0 0 "">
  <400 270 400 290 "" 0 0 0 "">
  <660 410 660 510 "" 0 0 0 "">
  <570 510 660 510 "" 0 0 0 "">
  <330 410 330 430 "" 0 0 0 "">
  <330 410 440 410 "" 0 0 0 "">
  <440 410 440 490 "" 0 0 0 "">
  <440 550 440 600 "" 0 0 0 "">
  <330 600 440 600 "" 0 0 0 "">
  <330 580 330 600 "" 0 0 0 "">
  <570 490 570 510 "" 0 0 0 "">
  <570 510 570 530 "" 0 0 0 "">
  <570 590 570 600 "" 0 0 0 "">
  <440 600 490 600 "" 0 0 0 "">
  <490 600 570 600 "" 0 0 0 "">
  <490 600 490 640 "" 0 0 0 "">
  <220 570 220 590 "" 0 0 0 "">
  <220 500 220 510 "" 0 0 0 "">
  <330 490 330 500 "" 0 0 0 "">
  <330 500 330 520 "" 0 0 0 "">
  <220 500 330 500 "" 0 0 0 "">
  <570 410 570 430 "" 0 0 0 "">
  <440 410 520 410 "" 0 0 0 "">
  <520 410 570 410 "" 0 0 0 "">
  <520 380 520 410 "" 0 0 0 "">
  <660 90 660 110 "" 0 0 0 "">
  <660 90 800 90 "" 0 0 0 "">
  <800 170 800 190 "" 0 0 0 "">
  <800 90 800 110 "" 0 0 0 "">
  <880 530 880 540 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
