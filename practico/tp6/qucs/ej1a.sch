<Qucs Schematic 0.0.18>
<Properties>
  <View=-240,15,865,680,1.4641,0,0>
  <Grid=10,10,1>
  <DataSet=ej1a.dat>
  <DataDisplay=ej1a.dpl>
  <OpenDisplay=1>
  <Script=ej1a.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Título>
  <FrameText1=Dibujado por:>
  <FrameText2=Fecha:>
  <FrameText3=Revisión:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <R R1 1 360 150 15 -26 0 1 "150 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <C C1 1 360 230 17 -26 0 1 "31.8 pF" 1 "" 0 "neutral" 0>
  <GND * 1 360 290 0 0 0 0>
  <GND * 1 210 290 0 0 0 0>
  <L L1 1 140 110 -26 10 0 0 "121.5 nH" 1 "" 0>
  <C C2 1 210 200 17 -26 0 1 "11.4 pF" 1 "" 0 "neutral" 0>
  <Pac P1 1 20 210 18 -26 0 1 "1" 0 "50 Ohm" 1 "1 mW" 1 "100 MHz" 1 "26.85" 0>
  <GND * 1 20 290 0 0 0 0>
  <.SP SP1 1 90 390 0 64 0 0 "lin" 1 "1 Hz" 1 "1000 MHz" 1 "5000" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
</Components>
<Wires>
  <360 110 360 120 "" 0 0 0 "">
  <210 110 360 110 "" 0 0 0 "">
  <360 180 360 200 "" 0 0 0 "">
  <360 260 360 290 "" 0 0 0 "">
  <170 110 210 110 "" 0 0 0 "">
  <210 110 210 170 "" 0 0 0 "">
  <210 230 210 290 "" 0 0 0 "">
  <20 110 110 110 "" 0 0 0 "">
  <20 110 20 180 "" 0 0 0 "">
  <20 240 20 290 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
