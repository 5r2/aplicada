<Qucs Schematic 0.0.18>
<Properties>
  <View=0,60,953,800,1.61051,0,187>
  <Grid=10,10,1>
  <DataSet=ej1b.dat>
  <DataDisplay=ej1b.dpl>
  <OpenDisplay=1>
  <Script=ej1b.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Título>
  <FrameText1=Dibujado por:>
  <FrameText2=Fecha:>
  <FrameText3=Revisión:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <GND * 1 640 410 0 0 0 0>
  <R R1 1 640 270 15 -26 0 1 "20 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <L L2 1 640 350 10 -26 0 1 "15.9 nH" 1 "" 0>
  <GND * 1 430 410 0 0 0 0>
  <L L1 1 510 230 -26 10 0 0 "23.1 nH" 1 "" 0>
  <Pac P1 1 240 330 18 -26 0 1 "1" 0 "50 Ohm" 1 "1 mW" 1 "100 MHz" 1 "26.85" 0>
  <GND * 1 240 410 0 0 0 0>
  <.SP SP1 1 380 520 0 64 0 0 "lin" 1 "1 Hz" 1 "1000 MHz" 1 "5000" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <C C2 1 430 320 17 -26 0 1 "39 pF" 1 "" 0 "neutral" 0>
</Components>
<Wires>
  <640 300 640 320 "" 0 0 0 "">
  <640 380 640 410 "" 0 0 0 "">
  <640 230 640 240 "" 0 0 0 "">
  <540 230 640 230 "" 0 0 0 "">
  <430 230 430 290 "" 0 0 0 "">
  <430 350 430 410 "" 0 0 0 "">
  <430 230 480 230 "" 0 0 0 "">
  <240 230 430 230 "" 0 0 0 "">
  <240 360 240 410 "" 0 0 0 "">
  <240 230 240 300 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
